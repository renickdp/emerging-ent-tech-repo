﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Helpers;
using System.Web;
using System.IO;
using SautinSoft.Document;
using System.Text;
using System.Data.SqlClient;
using System.Collections.Specialized;
using System.Threading;

namespace WebApplication2.Controllers
{
    public class NoteController : ApiController
    {

        // GET api/<controller>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            DataClassesDataContext ctx = new DataClassesDataContext();
            List<Note> notes = ctx.Notes.ToList();

            List<string> output = new List<string>();
            foreach (Note note in notes)
            {
                output.Add(System.Web.Helpers.Json.Encode(note));
            }
            return output;
        }

        // GET api/<controller>/5
        [HttpGet]
        public string Get(int id)
        {
            DataClassesDataContext ctx = new DataClassesDataContext();
            Note note = ctx.Notes.SingleOrDefault(u => u.id == id);
            return System.Web.Helpers.Json.Encode(note);
        }

        // POST api/<controller>
        [HttpPost]
        public IHttpActionResult Upload([FromUri] string userId)
        {
            HttpPostedFile file = HttpContext.Current.Request.Files.Count > 0 ?
        HttpContext.Current.Request.Files[0] : null;

            string fileName = file.FileName;

            string extension = Path.GetExtension(fileName);

            //Trust the extension. It's the windows way
            string content;
            if (extension == ".docx")
            {
                content = this.parseDocx(file);
            }
            else
            {
                content = this.parseTxt(file);
            }

            int createdID;
            try
            {
                createdID = this.writeNote(file, content, userId);
            }
            catch (SqlException error)
            {
                return BadRequest(error.Message);
            }
            return Ok(createdID);

        }

        // POST api/<controller>
        [HttpGet]
        public IHttpActionResult Stats([FromUri] int noteId)
        {
            string data = String.Format("{{\"file\": \"/home/ubuntu/wordcount.py\", \"args\": [\"{0}\"]}}", noteId);
            WebClient client = new WebClient();
            string output = client.UploadString("http://ec2-18-221-4-220.us-east-2.compute.amazonaws.com:8998/batches",
                                                               "POST", data);
            var batch = System.Web.Helpers.Json.Decode(output);
            string infoOutput;
            string lastLog;
            int id = -1;
            DynamicJsonArray log = null;

            string state = "running";
            while (state != "success" && state != "dead")
            {
                infoOutput = client.DownloadString("http://ec2-18-221-4-220.us-east-2.compute.amazonaws.com:8998/batches/" + batch.id.ToString());

                var batchInfo = System.Web.Helpers.Json.Decode(infoOutput);
                state = batchInfo.state;
                log = batchInfo.log;
                Thread.Sleep(200);
            }
            if (log.Length > 0 && state == "success")
            {
                lastLog = log[log.Length - 1];
                id = Int32.Parse(lastLog);
                return Ok(id);
            }
            else
            {
                return Content(HttpStatusCode.BadRequest, String.Join("\n", log));
            }
        }

        private int writeNote(HttpPostedFile file, string text, string userId)
        {
            string title = Path.GetFileName(file.FileName);
            DataClassesDataContext db = new DataClassesDataContext();
            Note note = new Note
            {
                noteownerID = userId,
                notetitle = title,
                text = text
            };
            db.Notes.InsertOnSubmit(note);
            db.SubmitChanges();
            return note.id;
        }

        private string parseDocx(HttpPostedFile file)
        {
            DocumentCore docx = DocumentCore.Load(file.FileName);

            StringBuilder text = new StringBuilder();

            foreach (Paragraph par in docx.GetChildElements(true, ElementType.Paragraph))
            {
                foreach (Run run in par.GetChildElements(true, ElementType.Run))
                {
                    text.Append(run.Text);
                }
                text.AppendLine();
            }

            return text.ToString();
        }

        private string parseTxt(HttpPostedFile file)
        {
            return new StreamReader(file.InputStream).ReadToEnd();
        }
    }
}