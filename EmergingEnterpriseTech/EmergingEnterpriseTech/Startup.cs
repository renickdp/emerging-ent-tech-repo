﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EmergingEnterpriseTech.Startup))]
namespace EmergingEnterpriseTech
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
