﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddNote.aspx.cs" Inherits="EmergingEnterpriseTech.AddNote" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        #form1 {
            height: 325px;
            width: 365px;
        }
    </style>
</head>
<body onunload="window.opener.document.location.href='Default.aspx';">
    <form id="form1" runat="server">
        Note title:<br />
        <asp:TextBox ID="NoteTitleTxtBox" runat="server"></asp:TextBox>
        <br />
        <br />
        Note:<br />
    <div>
    
        <asp:TextBox ID="NoteTxtBox" runat="server" Height="119px" MaxLength="260" Rows="10" style="margin-top: 0px" TextMode="MultiLine"></asp:TextBox>
        <br />
        <br />
        <asp:Button ID="AddNotePageBtn" runat="server" OnClick="AddNotePageBtn_Click" Text="Add Note" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="closeBtn" runat="server" OnClick="closeBtn_Click" Text="Close" />
        <br />
    
    </div>
    </form>
</body>
</html>
