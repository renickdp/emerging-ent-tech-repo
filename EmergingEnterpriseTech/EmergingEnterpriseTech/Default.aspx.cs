﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using System.IO;
using System.Runtime.InteropServices.ComTypes;
using SautinSoft.Document;
using System.Text;
using System.Net;
using System.Web.Helpers;

namespace EmergingEnterpriseTech
{
    public partial class _Default : Page
    {
        public static string SERVER_URL = "ec2-18-216-191-245.us-east-2.compute.amazonaws.com";
        private static WebClient client = new WebClient();
        private static int? noteId = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            string userId = User.Identity.GetUserId();
            if (userId == null)
            {
                userId = " ";
            }
            SqlDataSource1.FilterParameters[0].DefaultValue = userId;

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (_Default.noteId != null && TextBox1.Text != "")
            {
                this.makeAnalyzeRequest((int)_Default.noteId);
            }
        }

        protected void ListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedItem = ListBox1.GetSelectedIndices();

            int id = Int32.Parse(ListBox1.SelectedItem.Value);
            DataClasses1DataContext ctx = new DataClasses1DataContext();
            IQueryable<string> results = from note in ctx.Notes
                          where note.Id == id
                          select note.text;
            string text = results.Single();
            TextBox1.Text = text;
            _Default.noteId = id;
        }

        protected void SqlDataSource1_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            //  e.Command.Parameters["username"].Value = User.Identity.Name;
            
        }

        protected void SqlDataSource1_Filtering(object sender, SqlDataSourceFilteringEventArgs e)
        {
            //e.ParameterValues["@username"] = User.Identity.Name;
        }

        protected void AddNoteBtn_Click(object sender, EventArgs e)
        {
            string userId = User.Identity.GetUserId();
            if (userId == null)
            {
                Response.Write("  <script>alert('Please Log in before adding a note') </script>");
            }
            else
            {
                Response.Write("  <script language='javascript'> window.open('AddNote.aspx','','width=300,Height=400,fullscreen=1,location=0,scrollbars=1,menubar=1,toolbar=1'); </script>");
            }
        }

        protected void UploadBtn_Click(object sender, EventArgs e)
        {
            string extension = Path.GetExtension(FileUpload1.FileName);

            if (!string.IsNullOrEmpty(extension))
            {
                if (extension != ".docx" && extension != ".txt")
                {
                    Response.Write("  <script>alert('File must be .doc or .txt') </script>");
                }
                else
                {
                    string tmpPath = Path.Combine(Path.GetTempPath(),
                                                  FileUpload1.FileName);
                    FileUpload1.SaveAs(tmpPath);
                    this.makeUploadRequest(tmpPath);

                    Response.Redirect(Request.RawUrl);
                }
            }
            else
            {
                Response.Write("  <script>alert('Please select a file') </script>");

            }
        }    

        private void makeUploadRequest(string path)
        {
            FileStream file = File.OpenRead(path);
            string extension = Path.GetExtension(path);
            string mimeType = MimeMapping.GetMimeMapping(path);
            long fileLength = file.Length;
            long bytesRemaining = file.Length;
            byte[] bytes = new byte[fileLength];
            
            while (bytesRemaining > 0)
            {
                int bytesToRead = int.MaxValue;
                if (bytesToRead > fileLength)
                {
                    bytesToRead = (int)fileLength;
                }
                bytesRemaining -= file.Read(bytes, 0, bytesToRead);

            }
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("filename", file.Name);
            parms.Add("fileformat", extension);
            parms.Add("file", new FormUpload.FileParameter(bytes, file.Name, mimeType));

            string userID = User.Identity.GetUserId();
            string userAgent = "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/25.0";
            //http://localhost/api/note/upload/?userId
#if DEBUG
            string url = "http://" + _Default.SERVER_URL + "/NotesAPI/api/note/upload/?userId=" + userID;
#else
            string url = "http://" + _Default.SERVER_URL + ":8280/services/ApiProxy/upload/?userId=" + userID;
#endif
            FormUpload.MultipartFormDataPost(url, userAgent, parms);
        }
        private void makeAnalyzeRequest(int noteId)
        {
            //http://18.216.191.245:80/Notes
            int? id = null;
            for (int attempt = 0; attempt < 5; attempt++)
            {
#if DEBUG
                string url = "http://" + _Default.SERVER_URL + "/NotesAPI/api/note/stats/?noteId=" + noteId.ToString();
#else
            string url = "http://" + _Default.SERVER_URL + ":8280/services/ApiProxy/stats/?noteId=" + noteId.ToString();
#endif
                var response = _Default.client.DownloadString(url);

                id = Int32.Parse(response);
                if (id != -1)
                {
                    break;
                }
            }
            if (id == -1)
            {
                return;
            }
            DataClasses1DataContext ctx = new DataClasses1DataContext();
            Statistic stat = ctx.Statistics.SingleOrDefault(u => u.Id == id);
            string[] lines = { "Sentence Count: " + stat.sentencecount.ToString(),
                            "Word Count: " + stat.wordcount.ToString(),
                            "Syllable Count: " + stat.syllablecount.ToString(),
                            "Grade Level: " + stat.gradelevel.ToString() };
            string text = String.Join("\r\n", lines);
            AnalyzeTxtBox.Text = text;
        }
    }
}