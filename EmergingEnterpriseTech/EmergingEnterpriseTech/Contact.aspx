﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="EmergingEnterpriseTech.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2> Notes App .</h2>
    <h3>Your contact page.</h3>
    <address>
       Mercer University<br />
        Macon, GA 31080<br />
        <abbr title="Phone">P:</abbr>
        939.579.2795
    </address>

    <address>
        <strong>Support:</strong>   <a href="mailto:renickdp@hotmail.com">renickdp@hotmail.com</a><br />
    </address>
</asp:Content>
