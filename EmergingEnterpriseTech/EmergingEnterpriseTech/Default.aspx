﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="EmergingEnterpriseTech._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="jumbotron">
        <h1>Emerging Enterprise Technologies </h1>
        <p class="lead">&nbsp;</p>
        <p>&nbsp;</p>
    </div>

    <div class="row">
        <div class="col-md-4">
            <h2>Select your note</h2>
            <p>
                <asp:ListBox ID="ListBox1" runat="server" Height="151px" Width="195px" AutoPostBack="True" DataSourceID="SqlDataSource1" DataTextField="notetitle" DataValueField="Id" OnSelectedIndexChanged="ListBox1_SelectedIndexChanged"></asp:ListBox>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:EmergingEntTechDBConnectionString %>" FilterExpression="noteownerID='{0}'" OnFiltering="SqlDataSource1_Filtering" OnSelecting="SqlDataSource1_Selecting" SelectCommand="SELECT [notetitle], [Id], [noteownerID] FROM [Notes]">
                    <FilterParameters>
                        <asp:Parameter DbType="String" DefaultValue="" Name="@username_id" />
                    </FilterParameters>
                </asp:SqlDataSource>
&nbsp;</p>
            <p>
                <asp:Button ID="AddNoteBtn" runat="server" OnClick="AddNoteBtn_Click" Text="Add Note" Width="105px" />
&nbsp;&nbsp;&nbsp;&nbsp;
                </p>
            <p>
                &nbsp;</p>
            <p>
                Selext a txt or docx file to upload</p>
            <p>
                <asp:FileUpload ID="FileUpload1" runat="server" Height="21px" Width="226px" />
                <asp:Button ID="UploadBtn" runat="server" OnClick="UploadBtn_Click" Text="Upload note" />
            </p>
        </div>
        <div class="col-md-4">
            <h2>Note message</h2>
            <p>
                <asp:TextBox ID="TextBox1" runat="server" TextMode="MultiLine" Height="139px" Width="269px"></asp:TextBox>
            </p>
            <p>
                &nbsp;</p>
        </div>
        <div class="col-md-4">
            <h2>Analyze</h2>
            <p>
                &nbsp;<asp:TextBox ID="AnalyzeTxtBox" runat="server" Height="123px" Rows="6" TextMode="MultiLine" Width="169px"></asp:TextBox>
            </p>
            <p>
                <asp:Button ID="Button1" runat="server" Height="27px" OnClick="Button1_Click" Text="Analyze Text" />
            </p>
        </div>
    </div>

</asp:Content>
