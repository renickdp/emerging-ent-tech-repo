﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EmergingEnterpriseTech
{
    public partial class AddNote : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void AddNotePageBtn_Click(object sender, EventArgs e)
        {
            string noteTitle = NoteTitleTxtBox.Text;
            string noteText = NoteTxtBox.Text;

            string userId = User.Identity.GetUserId();

            if (string.IsNullOrEmpty(noteText) || string.IsNullOrEmpty(noteTitle))
            {
                Response.Write("  <script>alert('Please add a note and note title') </script>");
            }
            else
            {
                DataClasses1DataContext db = new DataClasses1DataContext();
                Note note = new Note
                {
                    noteownerID = userId,
                    notetitle = noteTitle,
                    text = noteText
                };
                db.Notes.InsertOnSubmit(note);
                //ssuming this won't fail for now
                db.SubmitChanges();
                Response.Write("  <script>alert('Note Added') </script>");

            }



        }

        protected void closeBtn_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close_Window", "self.close();", true);
        }
    }
}